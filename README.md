# API Init E21 -  Projet "Jeu Memory"   

## Commandes pour lancer le jeu ( avec Python3 )   

- Pour récupérer la version actuelle du jeu:    
    * Besoin d'installer tkinter:   
        Pour Python3 :
        ```bash
        sudo apt-get install python3-tk
        ```
    
    * Puis récupérer la version 1: git checkout Version_1
    
- Pour lancer le jeu :   
        ```bash
        python3 card.py   
        ```
