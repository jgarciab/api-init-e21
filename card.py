from tkinter import *
from random import shuffle

class Card():
    def __init__(self, parent, imageDir, nameCard, x, y):
        self.__name = nameCard
        self.__parent = parent

        self.__desk = PhotoImage(file= "{}/{}.gif".format(imageDir, "Deck" ))
        self.__face = PhotoImage(file= "{}/{}.gif".format(imageDir, nameCard ))
        self.__white = PhotoImage(file= "{}/{}.gif".format(imageDir, "white" ))
        
        self.show = Button(self.__parent.root, image=self.__desk, command=self.click)
        self.show.grid(row=y, column=x)
    def name(self):
        return self.__name
    def click(self):
        self.show.configure(image=self.__face)

        if self.__parent.clicked:
            self.__parent.root.after(500, lambda: self.compare(self.__parent.clicked))
        else:
            self.__parent.clicked = self

    def hide(self):
        self.show.configure(image=self.__desk)
    def disappear(self):
        self.show.configure(image=self.__white, state=DISABLED)
    def compare(self, card):
        self.__parent.clicked = None
        if card.name() != self.name():
            card.hide()
            self.hide()
            self.__parent.faux += 1
            self.__parent.v.set(self.__parent.faux)
        else:
            card.disappear()
            self.disappear()

class ListCard():
    def __init__(self, root):
        self.root = root
        self.clicked = None
        self.faux = 0
        
        # List card
        self.__listCcards = [ suit + rank for suit in "CDHS" for rank in "AK" ] * 2

        # button
        myFont = font.Font(size=8)
    
        Reboot = Button(root, text ="Réinitialiser", command = self.gui, width=7, height=2)
        Fin = Button(root, text ="Click \npour finir", command = self.close,  width=7, height=2)
        
        self.v = StringVar()
        self.compteur = Label(root, textvariable=self.v)
        

        Reboot['font'] = myFont
        Fin['font'] = myFont
        self.compteur['font'] = myFont

        Reboot.grid(row=5, column=2)
        Fin.grid(row=5, column=3)
        self.compteur.grid(row=5, column=0)

        # design interface
        self.gui()

        
    def gui(self):
        self.faux = 0
        self.v.set(self.faux)
        self.creatListCard()
        self.creatCard()
        

        # Board
        i = 0 # index of image in the list
        for x in range(4):
            for y in range(4):
                Card(self, "./images", listCards[i], x, y)
                i += 1

        


if __name__ == '__main__':
    root = Tk()
    ListCard(root)
    root.mainloop()